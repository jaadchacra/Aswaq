package com.aswaq.main.categories;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.aswaq.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.aswaq.models.Category;
import com.aswaq.models.Item;
import com.aswaq.utils.MySingleton;

public class CategoriesFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private ArrayList<Category> categoriesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_categories, container, false);
        mSavedInstanceState = savedInstanceState;

        init();

        getCategories(mSavedInstanceState);


        return view;
    }

    private void init() {
        progressBarCenter = (ProgressBar) view.findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) view.findViewById(R.id.studentFeedErrorMessage);
        retryButton = (Button) view.findViewById(R.id.studentFeedRetryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        categoriesList = new ArrayList<>();
    }




    private void getCategories(final Bundle savedInstanceState) {
        MySingleton.getInstance(getActivity()).addToRequestQueue(getAllCategoriesFromServer(savedInstanceState));
    }

    private JsonArrayRequest getAllCategoriesFromServer(final Bundle savedInstanceState) {
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://aswaq-api.herokuapp.com//api/v1/categories",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("allCategories", "1"+response.toString());
                        if(response != null){
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getCategories(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("allCategories", "error "+ error);
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getCategories(savedInstanceState);
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data about courses and put them in tinyDB
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            Category category = new Category();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
                category.setId(json.getInt("id"));
                category.setName(json.getString(("categoryName")));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            categoriesList.add(category);
        }
//
//        //Notifying the adapter that data has been added or changed
        adapter = new CategoriesAdapter(categoriesList, getActivity());
        recyclerView.setAdapter(adapter);

    }
}
