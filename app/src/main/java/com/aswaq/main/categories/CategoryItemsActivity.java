package com.aswaq.main.categories;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.aswaq.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.aswaq.general.ItemAdapter;
import com.aswaq.models.Category;
import com.aswaq.models.Item;
import com.aswaq.models.ItemColor;
import com.aswaq.utils.MySingleton;

public class CategoryItemsActivity extends AppCompatActivity {

    ArrayList<Item> itemsList = new ArrayList<Item>();
    ItemAdapter iAdapter;
    RecyclerView recyclerView;
    Bundle globalSavedInstanceState;

    ProgressBar progressBarCenter;
    TextView errorMessage,  text_toolbar_courses;
    Button retryButton;
    Toolbar toolbar;

    Category category;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_items);

        category = (Category) getIntent().getSerializableExtra("category");


        init();

        getCategoryItems(globalSavedInstanceState);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        iAdapter = new ItemAdapter(itemsList, this);
        recyclerView.setAdapter(iAdapter);

    }
    private void init() {
        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) findViewById(R.id.errorMessageTextView);
        retryButton = (Button) findViewById(R.id.retryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);


        text_toolbar_courses = (TextView) findViewById(R.id.text_toolbar_courses);
        text_toolbar_courses.setText(category.getName());
        toolbar = (Toolbar) findViewById(R.id.toolbar_courses);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



    private void getCategoryItems(final Bundle savedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getCategoryItemsFromServer(savedInstanceState));
    }

    private JsonArrayRequest getCategoryItemsFromServer(final Bundle savedInstanceState) {
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("https://aswaq-api.herokuapp.com/api/v1/categories/"+category.getId(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("allItems", "1"+response.toString());
                        if(response != null){
                            if(response.length() == 0)
                                Toast.makeText(CategoryItemsActivity.this, "No items in this category yet", Toast.LENGTH_SHORT).show();
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getCategoryItems(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("allItems", "error "+ error);
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getCategoryItems(savedInstanceState);
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data about courses and put them in tinyDB
    //This method will parse json data about courses and put them in tinyDB
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            Item item = new Item();
            ArrayList<ItemColor> itemColorArrayList = new ArrayList<>();
            JSONObject json = null;
            JSONObject imageJson = null;
            JSONObject imageJson2 = null;
            JSONArray colorJsonArray;
            try {
                //Getting json
                json = array.getJSONObject(i);
                imageJson = json.getJSONObject("itemphoto");
                imageJson2 = imageJson.getJSONObject("itemphoto");
                item.setItemImage(imageJson2.getString("url"));
                Log.d("imagagagaga", item.getItemImage());

//                item.setInCart(json.getBoolean("inOrder"));
                item.setItemId(json.getInt("id"));
                item.setItemDescription(json.getString(("itemdescription")));
                item.setItemModel(json.getString("itemmodel"));
                item.setItemSize(json.getInt("itemsize"));
                item.setItemStockqty(json.getInt("itemstockqty"));
                item.setItemPrice(json.getDouble("itemprice"));

                colorJsonArray = json.getJSONArray("colors");
                if(colorJsonArray != null){
                    JSONObject jsonObj = null;
                    for(int k = 0; k<colorJsonArray.length(); k++){
                        ItemColor itemColor = new ItemColor();
                        jsonObj = colorJsonArray.getJSONObject(k);
                        itemColor.setId(Integer.parseInt(jsonObj.getString("id")));
                        itemColor.setColorValue(jsonObj.getString("colorvalue"));
                        itemColorArrayList.add(itemColor);
                    }

                }

                item.setItemColors(itemColorArrayList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            itemsList.add(item);


        }
//
//        //Notifying the adapter that data has been added or changed
        iAdapter.notifyDataSetChanged();

    }

}
