package com.aswaq.main.categories;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aswaq.R;

import java.util.ArrayList;

import com.aswaq.models.Category;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {
    ArrayList<Category> categoriesList;
    private Context context;

    public CategoriesAdapter(ArrayList<Category> categoriesList, Context context){
        super();
        this.context = context;
        this.categoriesList = categoriesList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_item_category, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Category category = categoriesList.get(position);

        holder.categoryTextView.setText(category.getName());

        holder.relCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Making an intent to ItemDetails and passing an Item Object
                Intent myIntent = new Intent(context, CategoryItemsActivity.class);
                myIntent.putExtra("category", category);
                context.startActivity(myIntent);
                ((Activity)context).overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView categoryTextView;
        public RelativeLayout relCategory;
        public MyViewHolder(View view) {
            super(view);
            categoryTextView = (TextView) view.findViewById(R.id.categoryTextView);
            relCategory = (RelativeLayout) view.findViewById(R.id.relCategory);
        }
    }
}
