package com.aswaq.main.cart;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aswaq.R;
import com.aswaq.main.profile.billinginfos.BillingInfoActivity;
import com.aswaq.models.Item;
import com.aswaq.registration.profileimage.ImageViewActivity;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.TinyDB;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by iskandarchacra on 5/6/17.
 */

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.MyViewHolder> {
    ArrayList<Item> itemsList;
    private Context context;

    public CartItemAdapter(ArrayList<Item> itemsList, Context context){
        super();
        this.context = context;
        this.itemsList = itemsList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_item_cart, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Item item = itemsList.get(position);
        final String priceToString= Double.toString(item.getItemPrice());
//        final String sizeToString = Double.toString(item.getItemSize());
        holder.cartItemModel.setText(item.getItemModel());
        holder.cartItemPrice.setText("$"+priceToString);
        holder.cartItemColor.setText(""+item.getOrderColor());
        holder.cartItemQuantity.setText(item.getOrderQty());

        Picasso.with(context)
                .load(item.getItemImage())
                .into(holder.ipadplaceholder);

        holder.removeCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Remove item")
                        .setMessage("Are you sure you want to remove this item from your cart?")
                        .setCancelable(true)
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        removeFromCart(item);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return itemsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        //        public TextView itemDescription;
        public TextView cartItemColor;
        public TextView cartItemPrice;
        public TextView cartItemModel;
        public TextView cartItemQuantity;
        public ImageView ipadplaceholder;
        public Button removeCartButton;

        public MyViewHolder(View view) {
            super(view);
//            itemDescription = (TextView) view.findViewById(R.id.itemDescription);
            cartItemColor = (TextView) view.findViewById(R.id.cartItemColor);
            cartItemPrice = (TextView) view.findViewById(R.id.cartItemPrice);
            cartItemModel = (TextView) view.findViewById(R.id.cartItemModel);
            cartItemQuantity = (TextView) view.findViewById(R.id.cartItemQuantity);
            ipadplaceholder = (ImageView) view.findViewById(R.id.ipadplaceholder);
            removeCartButton = (Button) view.findViewById(R.id.removeCartButton);

        }
    }

    private void removeFromCart(Item item) {
        MySingleton.getInstance(context).addToRequestQueue(removeFromCartOnServer(item));
    }
    private StringRequest removeFromCartOnServer(final Item item) {
        TinyDB tinyDB = new TinyDB(context);
        String userID = tinyDB.getString("uid");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://aswaq-api.herokuapp.com/api/v1/remove/" + userID + "/" + item.getItemId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        itemsList.remove(item);
                        notifyDataSetChanged();
                        if(itemsList.size() == 0){
                            //make checkout button disappear
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(context, "Please try again", Toast.LENGTH_LONG).show();
                    }
                });
        return stringRequest;

    }
}
