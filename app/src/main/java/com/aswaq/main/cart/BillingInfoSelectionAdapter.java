package com.aswaq.main.cart;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aswaq.R;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import com.aswaq.main.MainActivity;
import com.aswaq.models.BillingInfo;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.TinyDB;

public class BillingInfoSelectionAdapter extends RecyclerView.Adapter<BillingInfoSelectionAdapter.MyViewHolder> {
    ArrayList<BillingInfo> billingInfoList;
    private Context context;

    public BillingInfoSelectionAdapter(ArrayList<BillingInfo> billingInfoList, Context context) {
        super();
        this.context = context;
        this.billingInfoList = billingInfoList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_item_billinginfo, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final BillingInfo billing = billingInfoList.get(position);

        holder.cardTextView.setText(""+billing.getCardnumber());
        holder.cvvTextView.setText(""+billing.getCcv());
        holder.expTextView.setText(""+billing.getExpiration());


        holder.relBillingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPurchase(billing);

            }
        });
    }

    @Override
    public int getItemCount() {
        return billingInfoList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cardTextView, cvvTextView, expTextView;
        public RelativeLayout relBillingInfo;

        public MyViewHolder(View view) {
            super(view);
            cardTextView = (TextView) view.findViewById(R.id.cardTextView);
            cvvTextView = (TextView) view.findViewById(R.id.cvvTextView);
            expTextView = (TextView) view.findViewById(R.id.expTextView);

            relBillingInfo = (RelativeLayout) view.findViewById(R.id.relBillingInfo);
        }
    }

    public void addPurchase(final BillingInfo billingInfo) {
        MySingleton.getInstance(context).addToRequestQueue(addPurchaseToServer(billingInfo));
    }
    private StringRequest addPurchaseToServer(final BillingInfo billingInfo) {
        Log.d("intentntne", "inina");
        //Making an intent to ItemDetails and passing an Item Object
        //TODO: Call empty payment with billing info
        final TinyDB tinyDB = new TinyDB(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://aswaq-api.herokuapp.com/api/v1/payments",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("resppsps", s);
                        tinyDB.putBoolean("emptied", true);
                        Intent myIntent = new Intent(context, MainActivity.class);
                        context.startActivity(myIntent);
                        ((Activity) context).overridePendingTransition(0, 0);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(context, "Please check your internet connection and try again", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("order_id", tinyDB.getString("orderID") );
                params.put("billing_info_id", billingInfo.getId()+"" );
                return params;
            }
        };
        return stringRequest;
    }
}
