package com.aswaq.main.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.aswaq.R;
import com.aswaq.main.profile.billinginfos.BillingInfoActivity;
import com.aswaq.models.Item;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CartFragment extends Fragment {
    RelativeLayout checkoutRel;
    ArrayList<Item> itemsList;
    CartItemAdapter iAdapter;
    RecyclerView recyclerView;
    View view;
    Bundle globalSavedInstanceState;
    TinyDB tinyDB;
    String userID;
    Button checkoutButton;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;

    @Override
    public void onResume() {
        Log.d("resusmeme", "reusme");
        if(tinyDB.getBoolean("emptied")) {
            tinyDB.putBoolean("emptied", false);
            reloadFragment();
        }
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cart, container, false);
        globalSavedInstanceState = savedInstanceState;

        tinyDB = new TinyDB(getActivity());
        userID = tinyDB.getString("uid");

        itemsList = new ArrayList<Item>();

        checkoutButton = (Button) view.findViewById(R.id.checkoutButton);
        checkoutButtonClickListener();

        checkoutRel = (RelativeLayout) view.findViewById(R.id.checkoutRel);
        checkoutRel.setVisibility(View.GONE);

        progressBarCenter = (ProgressBar) view.findViewById(R.id.progressBarCenter);
        errorMessage = (TextView) view.findViewById(R.id.errorMessageTextView);
        retryButton = (Button) view.findViewById(R.id.retryButton);
        progressBarCenter.setVisibility(View.GONE);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        recyclerView = (RecyclerView) view.findViewById(R.id.cartRecyclerView);

        getAllItems(globalSavedInstanceState);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(mLayoutManager);

        iAdapter = new CartItemAdapter(itemsList, getActivity());
        recyclerView.setAdapter(iAdapter);

        return view;
    }

    private void getAllItems(final Bundle savedInstanceState) {
        MySingleton.getInstance(getActivity()).addToRequestQueue(getAllItemsFromServer(savedInstanceState));
    }

    private JsonArrayRequest getAllItemsFromServer(final Bundle savedInstanceState) {

        Log.d("allItemscart", userID);
        progressBarCenter.setVisibility(View.VISIBLE);

        if (itemsList.size() == 0)
            checkoutRel.setVisibility(View.GONE);
        else
            checkoutRel.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("https://aswaq-api.herokuapp.com/api/v1/cart/" + userID,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("allItemscart", "1" + response.toString());
                        if (response != null) {
                            if(response.length() == 0)
                                Toast.makeText(getActivity(), "Your cart is empty", Toast.LENGTH_SHORT).show();
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getAllItems(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getAllItems(savedInstanceState);
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data about courses and put them in tinyDB
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            checkoutRel.setVisibility(View.VISIBLE);

            Item item = new Item();
            JSONObject json = null;

            try {
                //Getting json
                json = array.getJSONObject(i);
                item.setItemId(json.getInt("id"));
                item.setItemModel(json.getString("itemmodel"));
                item.setItemPrice(json.getDouble("itemprice"));
                item.setOrderColor(json.getString("color"));
                item.setOrderQty(json.getString("quantity"));
                item.setItemImage("http://res.cloudinary.com/zany/" + json.getString("itemphoto"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            itemsList.add(item);

        }
//
//        //Notifying the adapter that data has been added or changed
        iAdapter.notifyDataSetChanged();

    }

    private void checkoutButtonClickListener() {
        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BillingInfoSelectionActivity.class);
                startActivity(intent);
            }
        });
    }
    private void reloadFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}
