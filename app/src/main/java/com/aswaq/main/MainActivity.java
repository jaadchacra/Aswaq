package com.aswaq.main;

import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aswaq.R;
import com.aswaq.main.browse.BrowseFragment;
import com.aswaq.main.cart.CartFragment;
import com.aswaq.main.categories.CategoriesFragment;
import com.aswaq.main.profile.MyProfileFragment;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.SQLiteHandler;
import com.aswaq.utils.TinyDB;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private BottomBar mBottomBar;
    RelativeLayout relProfileToolbar, relFilterToolbar;
    TextView changingTextView;
    MyProfileFragment profileFragment;
    String userID;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tinyDB = new TinyDB(this);
        relProfileToolbar = (RelativeLayout) findViewById(R.id.relProfileToolbar);
        relFilterToolbar = (RelativeLayout) findViewById(R.id.relFilterToolbar);
        relProfileToolbar.setVisibility(View.GONE);
        changingTextView = (TextView) findViewById(R.id.changingTextView);
        changingTextView.setText("Aswaq");

        toolbar = (Toolbar) findViewById(R.id.toolbar_courses);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setVisibility(View.GONE);


        userID = tinyDB.getString("uid");
        getMyOrderID();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        mBottomBar = BottomBar.attachShy((CoordinatorLayout) findViewById(R.id.myCoordinator_courses),
                findViewById(R.id.viewpager), savedInstanceState);
        mBottomBar.setItems(R.menu.bottombar_menu);

        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemOne) {
                    viewPager.setCurrentItem(0, true);// The user selected item number one.
                    relFilterToolbar.setVisibility(View.VISIBLE);
                    relProfileToolbar.setVisibility(View.GONE);
                }
                if (menuItemId == R.id.bottomBarItemTwo) {
                    viewPager.setCurrentItem(1, true);// The user selected item number two.
                    relFilterToolbar.setVisibility(View.GONE);
                    relProfileToolbar.setVisibility(View.VISIBLE);
                }
//
                if (menuItemId == R.id.bottomBarItemThree) {
                    viewPager.setCurrentItem(2, true);// The user selected item number three.
                    relFilterToolbar.setVisibility(View.GONE);
                    relProfileToolbar.setVisibility(View.VISIBLE);
                }
                if (menuItemId == R.id.bottomBarItemFour) {
                    viewPager.setCurrentItem(3, true);// The user selected item number four.
                    relFilterToolbar.setVisibility(View.GONE);
                    relProfileToolbar.setVisibility(View.VISIBLE);
                }
            }



            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemOne) {
                    // The user reselected item number one, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemTwo) {
                    // The user reselected item number two, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemThree) {
                    // The user reselected item number three, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemFour) {
                    // The user reselected item number four, scroll your content to top.
                }
            }
        });

         /*
        *
        * THESE COLORS WILL ONLY BE TAKEN IF YOU HAVE MORE THAN
        * 3 items in res -> Menu -> bottombarbullshit
        * Otherwise Grey Bar
        *
        * */
        // Setting colors for different tabs when there's more than three of them.
        // You can set colors for tabs in three different ways as shown below.
        mBottomBar.mapColorForTab(0, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(1, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(2, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(3, ContextCompat.getColor(this, R.color.colorPrimary));
    }

    /*
    * View Pager adapter for the Tabs of SALES and STORES
    * */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        profileFragment = new MyProfileFragment();
        adapter.addFragment(new BrowseFragment(), "BROWSE");
        adapter.addFragment(new CategoriesFragment(), "CATEGORIES");
        adapter.addFragment(new CartFragment(), "CART");
        adapter.addFragment(profileFragment , "PROFILE");



        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Runtime.getRuntime().gc();
//    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Necessary to restore the BottomBar's state, otherwise we would
        // lose the current tab on orientation change.
        mBottomBar.onSaveInstanceState(outState);
    }


    private void getMyOrderID() {
        MySingleton.getInstance(this).addToRequestQueue(getSessionsFromServer());
    }
    private StringRequest getSessionsFromServer() {
        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, "https://aswaq-api.herokuapp.com/api/v1/users/"+userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("sessions", response.toString());
                        Log.d("sessions", userID);
                        if (response != null) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                tinyDB.putString("orderID", jsonObject.getString("id"));
                                Log.d("sessions", jsonObject.getString("id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                getMyOrderID();
                            }
                        } else {
                            getMyOrderID();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {


                        getMyOrderID();

                    }
                });
        return postRequest;
    }

}
