package com.aswaq.main.browse;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.aswaq.R;
import com.aswaq.general.ItemAdapter;
import com.aswaq.models.Item;
import com.aswaq.models.ItemColor;
import com.aswaq.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class BrowseFragment extends Fragment {

    ArrayList<Item> itemsList;
    ItemAdapter iAdapter;
    RecyclerView recyclerView;
    Bundle globalSavedInstanceState;
    View view;

    Spinner signSpinner, orderBySpinner, dateSignSpinner;
    EditText qtyEditText, dateEditText;
    Button searchControlButton;

    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;

    boolean isFiltered;
    DatePickerDialog datePickerDialog;

    String url = "https://aswaq-api.herokuapp.com/api/v1/search?search=itemprice&sign=";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_browse, container, false);
        globalSavedInstanceState = savedInstanceState;

        init();

        getAllItems(globalSavedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        iAdapter = new ItemAdapter(itemsList, getActivity());
        recyclerView.setAdapter(iAdapter);

        searchControlButtonClickListener();


        return view;
    }


    private void searchControlButtonClickListener() {
        searchControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFiltered){
                    isFiltered = false;
                    searchControlButton.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.searchbutton));
                    reloadFragment();
                } else{
                    isFiltered = true;
                    url = "https://aswaq-api.herokuapp.com/api/v1/search?search=itemprice&sign=";
                    String tempValue = signSpinner.getSelectedItem().toString();
                    String value = "";
                    for(int i=0; i<tempValue.length(); i++){
                        if(tempValue.charAt(i) == '=')
                            value+= "%3D";
                        else if(tempValue.charAt(i) == '<')
                            value+="%3C";
                        else
                            value+= "%3E";
                    }
                    String intValue = "value="+qtyEditText.getText().toString();
                    url+=value+"&"+intValue;
                    String dateSign = "&datesign=";
                    if(dateSignSpinner.getSelectedItem().toString().equals("before"))
                        dateSign+="%3C%3D";
                    else
                        dateSign+="%3E%3D";
                    url+=dateSign;

                    url+="&date="+dateEditText.getText().toString();

                    url+="&AD="+orderBySpinner.getSelectedItem().toString();

                    filterClicked();

                    searchControlButton.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.remove));
                }
            }
        });
    }

    private void filterClicked() {
        MySingleton.getInstance(getActivity()).addToRequestQueue(getFilterFromServer());
    }
    private JsonArrayRequest getFilterFromServer() {
        progressBarCenter.setVisibility(View.VISIBLE);
        Log.d("iirurr", url);
        itemsList.clear();
        iAdapter.notifyDataSetChanged();

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("allItems", "1"+response.toString());
                        if(response != null){
                            if(response.length() == 0)
                                Toast.makeText(getActivity(), "No items yielded from your search", Toast.LENGTH_SHORT).show();
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            filterClicked();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("allItems", "error "+ error);
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                filterClicked();
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }


    private void init() {
        isFiltered = false;
        itemsList = new ArrayList<Item>();
        progressBarCenter = (ProgressBar) view.findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) view.findViewById(R.id.errorMessageTextView);
        retryButton = (Button) view.findViewById(R.id.retryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        signSpinner = (Spinner) getActivity().findViewById(R.id.signSpinner);
        dateSignSpinner = (Spinner) getActivity().findViewById(R.id.dateSignSpinner);
        orderBySpinner = (Spinner) getActivity().findViewById(R.id.orderBySpinner);
        qtyEditText = (EditText) getActivity().findViewById(R.id.qtyEditText);
        dateEditText = (EditText) getActivity().findViewById(R.id.dateEditText);
        searchControlButton = (Button) getActivity().findViewById(R.id.searchControlButton);

        ArrayList<String> listSign = new ArrayList<String>();
        listSign.add("=");
        listSign.add(">=");
        listSign.add("<=");
        listSign.add(">");
        listSign.add("<");
        ArrayAdapter<String> adapterSign = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listSign);
        signSpinner.setAdapter(adapterSign);

        ArrayList<String> listdateSign = new ArrayList<String>();
        listdateSign.add("before");
        listdateSign.add("after");
        ArrayAdapter<String> adapterdateSign = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listdateSign);
        dateSignSpinner.setAdapter(adapterdateSign);


        ArrayList<String> listOrder = new ArrayList<String>();
        listOrder.add("asc");
        listOrder.add("desc");
        ArrayAdapter<String> adapterOrder = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listOrder);
        orderBySpinner.setAdapter(adapterOrder);

    }


    private void getAllItems(final Bundle savedInstanceState) {
        MySingleton.getInstance(getActivity()).addToRequestQueue(getAllItemsFromServer(savedInstanceState));
    }

    private JsonArrayRequest getAllItemsFromServer(final Bundle savedInstanceState) {
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("https://aswaq-api.herokuapp.com/api/v1/items",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("allItems", "1"+response.toString());
                        if(response != null){
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getAllItems(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("allItems", "error "+ error);
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getAllItems(savedInstanceState);
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data about courses and put them in tinyDB
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            Item item = new Item();
            ArrayList<ItemColor> itemColorArrayList = new ArrayList<>();
            JSONObject json = null;
            JSONObject imageJson = null;
            JSONObject imageJson2 = null;
            JSONArray colorJsonArray;
            try {
                //Getting json
                json = array.getJSONObject(i);
                imageJson = json.getJSONObject("itemphoto");
                imageJson2 = imageJson.getJSONObject("itemphoto");
                item.setItemImage(imageJson2.getString("url"));
                Log.d("imagagagaga", item.getItemImage());

//                item.setInCart(json.getBoolean("inOrder"));
                item.setItemId(json.getInt("id"));
                item.setItemDescription(json.getString(("itemdescription")));
                item.setItemModel(json.getString("itemmodel"));
                item.setItemSize(json.getInt("itemsize"));
                item.setItemStockqty(json.getInt("itemstockqty"));
                item.setItemPrice(json.getDouble("itemprice"));

                colorJsonArray = json.getJSONArray("colors");
                if(colorJsonArray != null){
                    JSONObject jsonObj = null;
                    for(int k = 0; k<colorJsonArray.length(); k++){
                        ItemColor itemColor = new ItemColor();
                        jsonObj = colorJsonArray.getJSONObject(k);
                        itemColor.setId(Integer.parseInt(jsonObj.getString("id")));
                        itemColor.setColorValue(jsonObj.getString("colorvalue"));
                        itemColorArrayList.add(itemColor);
                        Log.d("coororor", item.getItemModel()+": "+itemColor.getColorValue());
                    }

                }

                item.setItemColors(itemColorArrayList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            itemsList.add(item);


        }
//
//        //Notifying the adapter that data has been added or changed
        iAdapter.notifyDataSetChanged();

    }

    private void reloadFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}
