package com.aswaq.main.profile.billinginfos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aswaq.R;

import java.util.ArrayList;

import com.aswaq.models.BillingInfo;

public class BillingInfoAdapter extends RecyclerView.Adapter<BillingInfoAdapter.MyViewHolder> {
    ArrayList<BillingInfo> billingInfoList;
    private Context context;

    public BillingInfoAdapter(ArrayList<BillingInfo> billingInfoList, Context context) {
        super();
        this.context = context;
        this.billingInfoList = billingInfoList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_item_billinginfo, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final BillingInfo billing = billingInfoList.get(position);

        holder.cardTextView.setText(""+billing.getCardnumber());
        holder.cvvTextView.setText(""+billing.getCcv());
        holder.expTextView.setText(""+billing.getExpiration());


        holder.relBillingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Making an intent to ItemDetails and passing an Item Object
//                Intent myIntent = new Intent(context, BillingInfoDetailsActivity.class);
//                myIntent.putExtra("billing", billing);
//                context.startActivity(myIntent);
//                ((Activity) context).overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return billingInfoList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cardTextView, cvvTextView, expTextView;
        public RelativeLayout relBillingInfo;

        public MyViewHolder(View view) {
            super(view);
            cardTextView = (TextView) view.findViewById(R.id.cardTextView);
            cvvTextView = (TextView) view.findViewById(R.id.cvvTextView);
            expTextView = (TextView) view.findViewById(R.id.expTextView);

            relBillingInfo = (RelativeLayout) view.findViewById(R.id.relBillingInfo);
        }
    }
}
