package com.aswaq.main.profile.billinginfos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aswaq.R;
import com.aswaq.models.BillingInfo;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class BillingInfoActivity extends AppCompatActivity {
    Bundle mSavedInstanceState;
    Toolbar sessionDetailsToolbar;
    TextView toolbarTextView;
    private ArrayList<BillingInfo> billingInfoList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton, addBillingInfoButton;
    String userID;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_info);
//This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        init();

        addBillingInfoButtonClickListener();

        getBillingInfo(mSavedInstanceState);

    }

    private void addBillingInfoButtonClickListener() {
        addBillingInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layout = new LinearLayout(BillingInfoActivity.this);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText cardNumberEt = new EditText(BillingInfoActivity.this);
                cardNumberEt.setHint("16 digit card number");
                cardNumberEt.setInputType(InputType.TYPE_CLASS_NUMBER);

                final EditText ccvEt = new EditText(BillingInfoActivity.this);
                ccvEt.setHint("3 digit ccv");
                ccvEt.setInputType(InputType.TYPE_CLASS_NUMBER);

                final EditText expirationEt = new EditText(BillingInfoActivity.this);
                expirationEt.setHint("expiry, ex: 09/12");

                layout.addView(cardNumberEt);
                layout.addView(ccvEt);
                layout.addView(expirationEt);

                AlertDialog.Builder builder = new AlertDialog.Builder(BillingInfoActivity.this);
                builder.setView(layout)
                        .setTitle("Add a WishList")
                        .setMessage("Name your WishList")
                        .setCancelable(true)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO: Call function to Add the session AND Notify change for adapter
                        addBillingInfo(mSavedInstanceState, dialog, cardNumberEt.getText().toString(), ccvEt.getText().toString(), expirationEt.getText().toString());
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void init() {
        TinyDB tinyDB = new TinyDB(this);
        userID = tinyDB.getString("uid");

        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        errorMessage = (TextView) findViewById(R.id.errorMessageTextView);
        retryButton = (Button) findViewById(R.id.retryButton);
        progressBarCenter.setVisibility(View.GONE);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        addBillingInfoButton = (Button) findViewById(R.id.addBillingInfoButton);

        toolbarTextView = (TextView) findViewById(R.id.toolbarTextView);
        sessionDetailsToolbar = (Toolbar) findViewById(R.id.sessionDetailsToolbar);
        toolbarTextView.setText("Billing Information");
        setSupportActionBar(sessionDetailsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(sessionDetailsToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        billingInfoList = new ArrayList<>();
    }


    private void getBillingInfo(final Bundle savedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getBillingInfoFromServer(savedInstanceState));
    }

    private StringRequest getBillingInfoFromServer(final Bundle savedInstanceState) {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, "https://aswaq-api.herokuapp.com/api/v1/billing/" + userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("offerrs", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getBillingInfo(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("offerrs", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getBillingInfo(savedInstanceState);
                            }
                        });
                    }
                });
        return postRequest;
    }

    //This method will parse json data
    private void parseData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                BillingInfo billingInfo = new BillingInfo();

                JSONObject billingInfoJsonObject = null;

                try {
                    Log.d("jadofferrs", "in parse");
                    Log.d("jadofferrs", "in parse2");

                    //Getting json
                    billingInfoJsonObject = jsonArray.getJSONObject(i);

                    String id = billingInfoJsonObject.getString("id");
                    String expdate = billingInfoJsonObject.getString("billingInfoExp");
                    String cvv = billingInfoJsonObject.getString("BillingInfoCvv");
                    String cardnum = billingInfoJsonObject.getString("BillingInfoCardNumber");


                    billingInfo.setId(Integer.parseInt(id));
                    billingInfo.setCcv(Integer.parseInt(cvv));
                    billingInfo.setCardnumber(cardnum);
                    billingInfo.setExpiration(expdate);


                    Log.d("jadofferrs", "in parseEndSuccessful");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                billingInfoList.add(i, billingInfo);
            }
//
//        //Notifying the adapter that data has been added or changed
            adapter = new BillingInfoAdapter(billingInfoList, this);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addBillingInfo(final Bundle savedInstanceState, DialogInterface dialog, String cardName, String cvv, String exp) {
        MySingleton.getInstance(this).addToRequestQueue(addBillingInfoToServer(savedInstanceState, dialog, cardName, cvv, exp));
    }

    private StringRequest addBillingInfoToServer(Bundle savedInstanceState, final DialogInterface dialog, final String cardName, final String cvv, final String exp) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://aswaq-api.herokuapp.com/api/v1/billing_infos",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("resppsps", s);
                        dialog.cancel();
                        BillingInfo bilf = new BillingInfo();
                        bilf.setCardnumber(cardName);
                        bilf.setCcv(Integer.parseInt(cvv));
                        bilf.setExpiration(exp);
                        billingInfoList.add(bilf);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("user_id", userID);
                params.put("BillingInfoCardNumber", cardName);
                params.put("BillingInfoCvv", cvv);
                params.put("billingInfoExp", exp);

                return params;
            }
        };
        return stringRequest;

    }
}
