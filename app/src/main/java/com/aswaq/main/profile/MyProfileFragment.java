package com.aswaq.main.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aswaq.R;
import com.aswaq.main.profile.billinginfos.BillingInfoActivity;
import com.aswaq.utils.TinyDB;
import com.squareup.picasso.Picasso;

public class MyProfileFragment extends Fragment {

    RelativeLayout relBillingInfo;
    TextView profile_name;
    ImageView profile_picture;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_profile, container, false);

        TinyDB tinyDB = new TinyDB(getActivity());
        String url = "http://kcapplications.com/Aswaq/images/"+tinyDB.getString("email") + ".jpg";
        Log.d("urlrlrl", url);
        profile_picture = (ImageView) view.findViewById(R.id.profile_picture);
        Picasso.with(getActivity())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .into(profile_picture);

        profile_name = (TextView) view.findViewById(R.id.profile_name);
        profile_name.setText(tinyDB.getString("name"));

        relBillingInfo = (RelativeLayout) view.findViewById(R.id.relBillingInfo);
        relBillingInfoClickListener();
        return view;
    }

    private void relBillingInfoClickListener() {
        relBillingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), BillingInfoActivity.class);
                startActivity(myIntent);
                getActivity().overridePendingTransition(0, 0);
            }
        });
    }

}
