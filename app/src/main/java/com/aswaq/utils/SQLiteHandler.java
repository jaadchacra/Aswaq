package com.aswaq.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

    private SQLiteDatabase sqLiteDatabase;

    private static final String TAG = "Sqlidasfd";

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "aswaq_database";

    // Login table name
    private static final String TABLE_USER = "user";
    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_UID = "uid";
    private static final String KEY_TOKEN = "auth_token";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_IMAGE = "url";
    private static final String KEY_DOB = "dob";
    private static final String KEY_ORDER_ID = "orderID";




    // Cart table name
    private static final String TABLE_CART_ITEMS = "cartItems";
    // Cart Table Columns names
    private static final String KEY_ITEM_ID = "itemid"; //this is the id in the table
    private static final String KEY_ITEM_CREATED = "created_at";
    private static final String KEY_ITEM_DESCRIPTION = "itemdescription";
    private static final String KEY_ITEM_MODEL = "itemmodel";
    private static final String ITEM_ID = "id";     //this is the id from server
    private static final String KEY_ITEM_SIZE = "itemsize";
    private static final String KEY_ITEM_PRICE = "itemprice";
    private static final String KEY_ITEM_COLOR = "color";
    private static final String KEY_ITEM_IMAGE = "url";


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_ADDRESS + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_DOB + " TEXT,"
                + KEY_TOKEN + "TEXT UNIQUE,"
                + KEY_UID + " TEXT UNIQUE" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        String CREATE_CART_TABLE = "CREATE TABLE " + TABLE_CART_ITEMS + "("
                + KEY_ITEM_ID + " INTEGER PRIMARY KEY," + KEY_ITEM_DESCRIPTION + " TEXT,"
                + KEY_ITEM_MODEL + " TEXT," + ITEM_ID + " TEXT," + KEY_ITEM_SIZE + " TEXT,"
                + KEY_ITEM_CREATED + " TEXT," + KEY_ITEM_PRICE + " INTEGER,"
                + KEY_ITEM_COLOR + " TEXT,"
                + KEY_ITEM_IMAGE + " TEXT"
                + ")";
        db.execSQL(CREATE_CART_TABLE);
        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String name, String email, String uid, String address, String url, String dob, String token) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_UID, uid);
        values.put(KEY_ADDRESS, address);
        values.put(KEY_IMAGE, url);
        values.put(KEY_DOB, dob);
        values.put(KEY_TOKEN, token);

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }
    /**
     * Storing cart details in database
     * */
    public void addCartItem(String created_at, String itemdescription, String itemmodel, String id, String itemsize, String itemprice, String color, String url ) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_CREATED, created_at);
        values.put(KEY_ITEM_DESCRIPTION, itemdescription);
        values.put(KEY_ITEM_MODEL, itemmodel);
        values.put(ITEM_ID, id);
        values.put(KEY_ITEM_SIZE, itemsize);
        values.put(KEY_ITEM_PRICE, itemsize);
        values.put(KEY_ITEM_COLOR, itemprice);
        values.put(KEY_ITEM_IMAGE, url);

        // Inserting Row
        long mid = db.insert(TABLE_CART_ITEMS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + mid);
    }


    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("uid", cursor.getString(3));
            user.put("address", cursor.getString(4));
            user.put("url", cursor.getString(5));
            user.put("dob", cursor.getString(6));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    /**
     * Getting user data from database
     * */
    public ArrayList<HashMap<String, String>> getCartDetails() {
        ArrayList<HashMap<String, String>> cart = new ArrayList<HashMap<String, String>>();

        String selectQuery = "SELECT  * FROM " + TABLE_CART_ITEMS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            HashMap<String, String> item = new HashMap<>();

            item.put("created_at", cursor.getString(1));
            item.put("itemdescription", cursor.getString(2));
            item.put("itemmodel", cursor.getString(3));
            item.put("id", cursor.getString(4));
            item.put("itemsize", cursor.getString(5));
            item.put("itemprice", cursor.getString(6));
            item.put("color", cursor.getString(7));
            item.put("url", cursor.getString(8));

            cart.add(item);
            while(cursor.moveToNext()){
                item = new HashMap<>();

                item.put("created_at", cursor.getString(1));
                item.put("itemdescription", cursor.getString(2));
                item.put("itemmodel", cursor.getString(3));
                item.put("id", cursor.getString(4));
                item.put("itemsize", cursor.getString(5));
                item.put("itemprice", cursor.getString(6));
                item.put("color", cursor.getString(7));
                item.put("url", cursor.getString(8));

                cart.add(item);
            }
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + cart.toString());

        return cart;
    }

    /**
     * Edits the credits by adding or subtracting depending on boolean value
     * */
    public void editUser(String address){
        ContentValues values = new ContentValues();
        values.put(KEY_ADDRESS, address);

//        sqLiteDatabase.update(TABLE_USER, values, null, null);
        String updateQuery = "UPDATE " + TABLE_USER + " SET "+ KEY_ADDRESS +" = "+ address;
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL(updateQuery);
    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

}
