package com.aswaq.registration;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aswaq.R;
import com.aswaq.login.LoginActivity;
import com.aswaq.main.MainActivity;
import com.aswaq.registration.profileimage.ImageViewActivity;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.SQLiteHandler;
import com.aswaq.utils.SessionManager;
import com.aswaq.utils.TinyDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends Activity {
    private static final String TAG = "reggegeg";
    private Button btnRegister;
//    private Button btnLinkToLogin;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText addressEditText;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    String first_name;
    String last_name;
    String email;
    String password;
    String address;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Log.d("ActivityNow", "I'm in RegisterActivity!");

        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
//        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());
        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                first_name = firstNameEditText.getText().toString().trim();
                last_name = lastNameEditText.getText().toString().trim();

                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();

                address = addressEditText.getText().toString().trim();

                if (!first_name.isEmpty() && !last_name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                    registerUser(first_name, last_name, email, address, password);
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter your details!", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Link to Login Screen
//        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View view) {
//                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });

    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     */
    private void registerUser(final String first_name, final String last_name, final String email, final String address,
                              final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST, " http://aswaq-api.herokuapp.com//api/v1/registrations", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");
                    Log.d(TAG, success);
                    if (success.equals("true")) {
                        Log.d(TAG, "inn"+success);

                        JSONObject data = jObj.getJSONObject("data");
                        Log.d(TAG, "data"+success);

                        String token = data.getString("auth_token");
                        Log.d(TAG, "token"+token);

                        JSONObject user = data.getJSONObject("user");
                        Log.d(TAG, "user"+success);

                        // User successfully stored in MySQL
                        // Now store the user in sqlite

                        String uid = user.getString("id");
                        String name = first_name +" "+ last_name;

                        Log.d(TAG, "inn"+success);

                        // Inserting row in users table
                        db.addUser(name, email, uid, address, "imgurl","dob", token);
                        Log.d(TAG, "db"+success);

                        TinyDB tinyDB = new TinyDB(getApplicationContext());
                        tinyDB.putString("name", name);
                        tinyDB.putString("email", email);
                        tinyDB.putString("uid", uid);
                        tinyDB.putString("auth_token", token);
                        tinyDB.putString("address", address);

                        session.setLogin(true);
                        // Launch login activity
                        Intent intent = new Intent(RegisterActivity.this, ImageViewActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error message
//                        String errorMsg = jObj.getString("error_msg");
//                        Toast.makeText(getApplicationContext(), "error"+errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "error"+error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("userfirstname", first_name);
                params.put("userlastname", last_name);
                params.put("useraddress", address);
                params.put("password", password);
                params.put("password_confirmation", password);

                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MySingleton.getInstance(this).addToRequestQueue(strReq);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}