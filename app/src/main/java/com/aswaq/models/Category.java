package com.aswaq.models;

import java.io.Serializable;

/**
 * Created by ATOM on 5/6/2017.
 */

public class Category implements Serializable {
    int id;
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
