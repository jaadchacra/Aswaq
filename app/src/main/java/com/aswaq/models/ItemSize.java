package com.aswaq.models;

import java.io.Serializable;

/**
 * Created by iskandarchacra on 3/29/17.
 */

public enum ItemSize implements Serializable {
    S,M,L, XL,XXL;
    public String getItemSize(){
        return this.name();
    }

}

