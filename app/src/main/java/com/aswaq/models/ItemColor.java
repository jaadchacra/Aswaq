package com.aswaq.models;

import java.io.Serializable;

/**
 * Created by ATOM on 5/6/2017.
 */

public class ItemColor  implements Serializable {
    int id;
    String colorValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }
}
