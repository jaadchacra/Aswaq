package com.aswaq.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by iskandarchacra on 3/29/17.
 */

public class Item implements Serializable {
    int itemId;
    Date itemDate;
    String itemDescription;
    String itemModel;
    int itemSize;
    int itemStockqty;

    public String getOrderColor() {
        return orderColor;
    }

    public void setOrderColor(String orderColor) {
        this.orderColor = orderColor;
    }

    String orderColor;
    String orderQty;

    public String getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(String orderQty) {
        this.orderQty = orderQty;
    }

    public boolean isInCart() {
        return isInCart;
    }

    public void setInCart(boolean inCart) {
        isInCart = inCart;
    }

    boolean isInCart;
    double itemPrice;
    String itemImage;
    ArrayList<Branch> itemBranches;
    ArrayList<ItemColor> itemColors;

    public ArrayList<Branch> getItemBranches() {
        return itemBranches;
    }

    public void setItemBranches(ArrayList<Branch> itemBranches) {
        this.itemBranches = itemBranches;
    }



    public ArrayList<ItemColor> getItemColors() {
        return itemColors;
    }

    public void setItemColors(ArrayList<ItemColor> itemColors) {
        this.itemColors = itemColors;
    }


    public int getItemSize() {
        return itemSize;
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }

    public Item() {
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemModel() {
        return itemModel;
    }

    public void setItemModel(String itemModel) {
        this.itemModel = itemModel;
    }

    public int getItemStockqty() {
        return itemStockqty;
    }

    public void setItemStockqty(int itemStockqty) {
        this.itemStockqty = itemStockqty;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public Date getItemDate() {
        return itemDate;
    }

    public void setItemDate(Date itemDate) {
        this.itemDate = itemDate;
    }
}
