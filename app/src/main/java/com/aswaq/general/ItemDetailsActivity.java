package com.aswaq.general;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.aswaq.R;
import com.aswaq.models.Item;
import com.aswaq.models.ItemColor;
import com.aswaq.utils.MySingleton;
import com.aswaq.utils.TinyDB;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;


public class ItemDetailsActivity extends AppCompatActivity {
    RelativeLayout qtyRel, colorRel, relEverything;
    TextView itemModel, itemDescription, priceTextView;
    Toolbar toolbar;
    ImageView itemImage;
    Button addToCartButton;
    Spinner my_spin;
    Item item;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;
    TinyDB tinyDB;
    String userID;
    EditText qtyEditText;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        // Getting item that we passed from ItemAdapter
        Intent mIntent = getIntent();
        item = (Item) mIntent.getSerializableExtra("item");

        tinyDB = new TinyDB(this);

        userID = tinyDB.getString("uid");

        relEverything = (RelativeLayout) findViewById(R.id.relEverything);
        relEverything.setVisibility(View.GONE);



        init();

        checkIfInCart();

        addToCartButtonClickListener();


    }

    private void initSpinner() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Choose Color");
        for (int i = 0; i < item.getItemColors().size(); i++) {
            Log.d("itemdetailsadapaterr", item.getItemModel() + ": " + item.getItemColors().get(i).getColorValue());
            list.add(item.getItemColors().get(i).getColorValue());
        }

        for (int z = 0; z < item.getItemColors().size(); z++) {
            Log.d("itemdetailsadaproror", item.getItemModel() + ": " + item.getItemColors().get(z).getColorValue());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        my_spin.setAdapter(adapter);
    }

    private void addToCartButtonClickListener() {
        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!item.isInCart()) {
                    if(my_spin.getSelectedItemPosition() != 0 && !qtyEditText.getText().toString().replaceAll(" ","").equals("")){
                        addToCart();
                    }else
                        Toast.makeText(ItemDetailsActivity.this, "Please choose color and quantity", Toast.LENGTH_SHORT).show();
                }
                else
                    removeFromCart();
            }
        });
    }


    public void init() {
        relEverything.setVisibility(View.VISIBLE);
        qtyRel = (RelativeLayout) findViewById(R.id.qtyRel);
        colorRel = (RelativeLayout) findViewById(R.id.colorRel);
        itemModel = (TextView) findViewById(R.id.itemModel);
        itemDescription = (TextView) findViewById(R.id.itemDetailsDescription);
        priceTextView = (TextView) findViewById(R.id.priceTextView);
        itemImage = (ImageView) findViewById(R.id.itemImage);
        addToCartButton = (Button) findViewById(R.id.addToCartButton);
        my_spin = (Spinner) findViewById(R.id.my_spin);
        qtyEditText = (EditText) findViewById(R.id.qtyEditText);

        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        errorMessage = (TextView) findViewById(R.id.errorMessageTextView);
        retryButton = (Button) findViewById(R.id.retryButton);
        progressBarCenter.setVisibility(View.GONE);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        itemModel.setText(item.getItemModel());
        itemDescription.setText(item.getItemDescription());
        priceTextView.setText("" + item.getItemPrice());

        initToolbar();

        cartLayout();



        Picasso.with(this)
                .load(item.getItemImage())
                .placeholder(R.drawable.placeholder)
                .into(itemImage);

    }

    private void cartLayout() {
        if (item.isInCart()) {
            qtyRel.setVisibility(View.GONE);
            colorRel.setVisibility(View.GONE);

            addToCartButton.setBackgroundColor(Color.GRAY);
            addToCartButton.setText("REMOVE FROM CART");
        } else {
            qtyRel.setVisibility(View.VISIBLE);
            colorRel.setVisibility(View.VISIBLE);
            initSpinner();
            addToCartButton.setBackgroundColor(Color.RED);
            addToCartButton.setText("ADD TO CART");
        }
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_courses);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void checkIfInCart() {
        MySingleton.getInstance(this).addToRequestQueue(checkIfInCartFromServer());

    }
    private StringRequest checkIfInCartFromServer() {
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        StringRequest jsonArrayRequest = new StringRequest("https://aswaq-api.herokuapp.com/api/v1/check/"+item.getItemId()+"/"+tinyDB.getString("orderID"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("allItems", "1"+response.toString());
                        if(response != null){
                            try {
                                Log.d("allItems", "in resp");
                                JSONObject jsonObject = new JSONObject(response);
                                if(jsonObject.getString("inOrder?").equals("true"))
                                    item.setInCart(true);
                                else
                                    item.setInCart(false);
                                init();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressBarCenter.setVisibility(View.VISIBLE);
                                checkIfInCart();
                            }


                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            checkIfInCart();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("allItems", "error "+ error);
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                checkIfInCart();
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }


















    public void addToCart() {
        MySingleton.getInstance(this).addToRequestQueue(addToCartToServer());
    }
    private StringRequest addToCartToServer() {
        progressBarCenter.setVisibility(View.VISIBLE);

        Log.d("addremv", "user: "+userID);
        Log.d("addremv", "itemID: "+item.getItemId());
        Log.d("addremv", "color: "+my_spin.getSelectedItem().toString());
        Log.d("addremv", "qty: "+qtyEditText.getText().toString());
        Log.d("addremv", "url: "+"https://aswaq-api.herokuapp.com/api/v1/add/" + userID + "/" + item.getItemId() + "/" + my_spin.getSelectedItem().toString() +"/"+ qtyEditText.getText().toString());


        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://aswaq-api.herokuapp.com/api/v1/add/" + userID + "/" + item.getItemId() + "/" + my_spin.getSelectedItem().toString() +"/"+ qtyEditText.getText().toString(),
               new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("addremv", "in: "+s);

                        try{
                            JSONObject jsonObj = new JSONObject(s);
                            String failed = jsonObj.getString("failed");
                            Log.d("addremv", "try: "+s);

                            if(failed==null || failed.equals("false")){
                                Log.d("addremv", "add: "+s);
                                item.setInCart(true);
                                cartLayout();
                            } else
                                Toast.makeText(ItemDetailsActivity.this, "failed "+failed, Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            Toast.makeText(ItemDetailsActivity.this, "catch "+e, Toast.LENGTH_SHORT).show();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("addremv", "add: "+volleyError);

                        if (volleyError instanceof NoConnectionError) {

                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else{
                            Toast.makeText(getApplicationContext(), "There aren't that many items in stock. Please select less", Toast.LENGTH_LONG).show();

                        }
                    }
                });
        return stringRequest;
    }

    private void removeFromCart() {
        MySingleton.getInstance(this).addToRequestQueue(removeFromCartOnServer());
    }
    private StringRequest removeFromCartOnServer() {
        progressBarCenter.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://aswaq-api.herokuapp.com/api/v1/remove/" + userID + "/" + item.getItemId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("addremv", "rmv: "+s);
                        item.setInCart(false);
                        cartLayout();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return stringRequest;

    }


}
