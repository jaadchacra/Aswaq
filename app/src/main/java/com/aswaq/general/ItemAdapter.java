package com.aswaq.general;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aswaq.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.aswaq.models.Item;

/**
 * Created by iskandarchacra on 3/29/17.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyViewHolder> {
    ArrayList<Item> itemsList;
    private Context context;

    public ItemAdapter(ArrayList<Item> itemsList, Context context){
        super();
        this.context = context;
        this.itemsList = itemsList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_item_product, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Item item = itemsList.get(position);
        final String priceToString= Double.toString(item.getItemPrice());
//        holder.itemLayout.setOnClickListener();
        holder.itemDescription.setText(item.getItemModel());
        holder.itemPrice.setText("$"+priceToString);

        Picasso.with(context)
                .load(item.getItemImage())
                .into(holder.ipadplaceholder);

        holder.relItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Making an intent to ItemDetails and passing an Item Object
                Intent myIntent = new Intent(context, ItemDetailsActivity.class);
                myIntent.putExtra("item", item);
                context.startActivity(myIntent);
                ((Activity)context).overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView itemDescription;
        public RelativeLayout relItemImage;
        public TextView itemPrice;
        public ImageView ipadplaceholder;

        public MyViewHolder(View view) {
            super(view);
            itemDescription = (TextView) view.findViewById(R.id.itemDescription);
            relItemImage = (RelativeLayout) view.findViewById(R.id.relItemImage);
            itemPrice = (TextView) view.findViewById(R.id.itemPrice);
            ipadplaceholder = (ImageView) view.findViewById(R.id.ipadplaceholder);
        }
    }
}
